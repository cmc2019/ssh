FROM alpine

RUN \
    apk --no-cache add \
    openssh \
    socat \
    autossh \
    rsync \
    netcat-openbsd \
    sshfs
